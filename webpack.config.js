const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

module.exports = (env, options) => ({
    entry: {
        main: path.resolve(__dirname, './app/index.js'),
        secondary: path.resolve(__dirname, './app/secondary'),
    },
    output: {
        path: path.resolve(__dirname, './dist/assets'),
        filename: options.mode === 'production' ? '[name].[contenthash].js' : '[name].js',
        publicPath: '/assets',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$|\.tsx?$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.css$/,
                use: [
                    options.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
                    'css-loader',
                    'postcss-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    options.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: path.resolve(__dirname, './config/_config.scss'),
                        },
                    },
                ],
            }
        ],
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './app/index.html'),
            filename: '../index.html',
            alwaysWriteToDisk: true,
        }),
        new HtmlWebpackHarddiskPlugin(),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
        }),
    ],
    optimization: {
        minimizer: [
            '...',
            new CssMinimizerPlugin(),
        ],
        splitChunks: {
            chunks: 'all',
            name(module, chunks, cacheGroupKey) {
                const moduleFileName = module
                    .identifier()
                    .split('/')
                    .reduceRight((item) => item);
                const allChunksNames = chunks.map((item) => item.name).join('~');
                return `${cacheGroupKey}-${allChunksNames}-${moduleFileName}`;
            },
        },
    },
    resolve: {
        alias: {},
        extensions: [
            '.jsx',
            '.js',
            '.tsx',
            '.ts'
        ],
    }
})
