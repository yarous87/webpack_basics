import React, { useState } from 'react';

export const App: React.FC = () => {
    const [state, setState] = useState("CLICK ME");

    return <button onClick={() => setState("CLICKED")}>{state}</button>;
}
