import amd from './amd';
import commonjs from './common';
import esm from './esm';
import React from 'react';
import ReactDOM from 'react-dom';

import './test.css';
import './index.scss';

amd();
commonjs();
esm();

const getApp = import(/* webpackChunkName: "app-component" */ './app');

getApp.then(({ App }) => {
    ReactDOM.render(<App />, document.getElementById('root'));
});
